package com.petowner.service;

import java.util.List;

import com.petowner.entity.Pet;

public interface PetService {

	List<Pet> loadPets();

}