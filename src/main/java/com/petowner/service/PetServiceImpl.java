package com.petowner.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.petowner.entity.Pet;
import com.petowner.repository.PetRepository;

@Service
@Transactional(readOnly = true)
public class PetServiceImpl implements PetService {

	@Autowired
	private PetRepository petRepository;

	/* (non-Javadoc)
	 * @see com.petowner.service.PetService#loadPets()
	 */
	@Override
	public List<Pet> loadPets() {
		List<Pet> pets = petRepository.findAll();

		pets.forEach(pet -> pet.getOwner().getFirstName());
		return pets;

	}

}
